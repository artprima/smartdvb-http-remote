﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

using HINSTANCE = System.IntPtr;
using HWND = System.IntPtr;
using HMENU = System.IntPtr;

namespace ChannelSwitchCS
{

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public unsafe struct AddOnSettings
    {
        public HINSTANCE hInst; // SmartDVB application instance
        public HWND hwnd;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string name; // addon name
        public HMENU menu;      // addon popup menu
        public ushort idmenustart;
    };


    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public unsafe struct DeviceSettings
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string tunername;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string modulename;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string sId;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public unsafe struct EXTRACHANINFO
    {
        public uint iFilePos; //ckey ???
        public ushort category;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public unsafe struct ESTREAM //28
    {
        public ushort type; //2
        public ushort PID; //2
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] language;//4
        //[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
        //public string language;
        public int bIsAC3;//4
        public int bIsTTX;//4
        public int bIsAAC;//4
        public int bIsDVBSUB;//4
        public int bIsHEVC;//4
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public unsafe struct CASYSTEM
    {
        public ushort CAType;
        public ushort ECM;
        public ushort EMM;
    }


    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public unsafe struct CHANNEL030
    {
        public ushort SID;
        public ushort TID;
        public ushort PMT_PID;
        public ushort PCR_PID;
        public ushort VID_PID;
        public ushort AUD_PID;
        public ushort AC3_PID;
        public ushort TTX_PID;
        public ushort ECM_PID;
        public ushort DVBSUB_PID; // dvb subtitling pid, growth1 in db
        public ushort CH;
        public int NID;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public ESTREAM[] ES;
        public ushort iESCount;	// # of elementary streams
        public int bHasService;			// we have an sdt entry for this
        public int bHasCA;				// true if we have at least 1 CA
        public int bHasAC3;				// true if we have at least 1 AC3
        public int bHasH264;				// true if we have h264 video channel
        public int bHasDVBSUB;				// true if we have h264 video channel
        public int bDeleted;
        public int bUseAC3;
        public int bUseAAC;
        public int bAutoFec;
        public int iSourceId;		// atsc source id, dev_id in database
        public byte ServiceType;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 65)]
        public string ServiceProviderName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 65)]
        public string ServiceName;
        public byte iCACount;	    // conditional access count
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public CASYSTEM[] CASystem;
        public ushort iSatID;
        public TRANSPONDER Transponder;
        public EXTRACHANINFO extra;
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst=988)]
        //public byte[] Satellite;
        public SATELLITE Satellite;
        public ushort Modulation;
        public ushort ttxsubtitlepage;
        public short DefaultCodepage;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
        public string DefaultEPGLang;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct SATELLITE
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
        public string Name;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 10)]
        public string Pos;
        public ushort ID;
        public byte DevNr;
        public int lof1, lof2, sw;
        public int diseqctype1, diseqclnb1;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string rawcmd;
        public int diseqctype2, diseqclnb2;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string rawcmd2;
        public ushort lnbtype;
        public int positionertype;
        public int positionerpos;
        public int positionerangle;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string rawcmdpositioner;
        public bool b22khz;
        public int diseqc1tx, diseqc2tx, positionertx,
        diseqc1gap, diseqc2gap, positionergap,
        lnbdelay;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct TRANSPONDER
    {
        public uint frequency;
        public uint symbolrate;
        public byte polarity;
        public uint fec;      // transponderfec values
        public uint id;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct OSDWindowType
    {
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct OSDWindowInfo
    {
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct IAddOnOSDEvents
    {
    }

    class Structs
    {
    }
}
