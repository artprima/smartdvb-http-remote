﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using Newtonsoft.Json;

namespace ChannelSwitchCS
{
    public class HostPort
    {
        public string host { get; set; }
        public string port { get; set; }
    }

    public class Config
    {
        [YamlMember(Alias="bind-to")]
        public List<HostPort> BindTo { get; set; }

        private string _allowCors = "no";

        [YamlMember(Alias="allow-cors")]
        public string AllowCors { 
            get {
                return _allowCors;
            }
            set {
                switch (value) { 
                    case "no":
                    case "yes":
                    case "all":
                        _allowCors = value;
                        break;
                    default:
                        _allowCors = "no";
                        break;
                }
            }
        }

        [YamlMember(Alias="cors-prefixes")]
        public List<string> CorsPrefixes { get; set; }
    }

    public class ConfigProcessor
    { 
        private Config config;

        private void AddText(FileStream fs, string value)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(value);
            fs.Write(info, 0, info.Length);
        }
        private void AddLine(FileStream fs, string value)
        { 
            AddText(fs, value + "\r\n");
        }

        public ConfigProcessor(string path)
        {
            if (!System.IO.File.Exists(path)) { 
                Directory.CreateDirectory(Path.GetDirectoryName(path));
                using (FileStream writer = File.Create(path)) { 
                    AddLine(writer, "bind-to:");
                    AddLine(writer, "    - { host: localhost, port: 8888 }");
                    AddLine(writer, "allow-cors: no");
                    AddLine(writer, "cors-prefixes: ~");
                }
            }
            //MessageBox.Show(path);
            try {
                StreamReader reader = new StreamReader(path, Encoding.UTF8);
                Deserializer deserializer = new Deserializer(namingConvention: new CamelCaseNamingConvention(), ignoreUnmatched: true);
                config = deserializer.Deserialize<Config>(reader);
            } catch (Exception e) {
                MessageBox.Show(JsonConvert.SerializeObject(e));
                config = new Config();
                config.BindTo.Add(new HostPort { host = "localhost", port = "8888" });
            }
            //MessageBox.Show(JsonConvert.SerializeObject(config));
        }

        public Config GetConfig()
        { 
            return config;
        }
    }
}

