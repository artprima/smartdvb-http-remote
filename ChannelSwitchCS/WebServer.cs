﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;

namespace ChannelSwitchCS
{

    public class WebServer
    {
        private readonly HttpListener _listener = new HttpListener();
        private readonly Func<HttpListenerContext, string> _responderMethod;
        private readonly string _allowCors;
        private readonly List<string> _corsPrefixes;

        public WebServer(string[] prefixes, Func<HttpListenerContext, string> method, string allowCors, List<string> corsPrefixes)
        {
            if (!HttpListener.IsSupported)
                throw new NotSupportedException(
                    "Needs Windows XP SP2, Server 2003 or later.");

            // URI prefixes are required, for example 
            // "http://localhost:8080/index/".
            if (prefixes == null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");

            // A responder method is required
            if (method == null)
                throw new ArgumentException("method");

            foreach (string s in prefixes) {
                _listener.Prefixes.Add(s);
            }

            _allowCors = allowCors;
            _corsPrefixes = corsPrefixes;

            _responderMethod = method;
            _listener.Start();
        }

        public WebServer(Func<HttpListenerContext, string> method, string allowCors, List<string>corsPrefixes, params string[] prefixes)
            : this(prefixes, method, allowCors, corsPrefixes) { }

        public void Run()
        {
            ThreadPool.QueueUserWorkItem((o) =>
            {
                //Console.WriteLine("Webserver running...");
                try
                {
                    while (_listener.IsListening)
                    {
                        ThreadPool.QueueUserWorkItem((c) =>
                        {
                            var ctx = c as HttpListenerContext;
                            try
                            {
                                string rstr = _responderMethod(ctx);
                                byte[] buf = Encoding.UTF8.GetBytes(rstr);
                                
                                switch (_allowCors) { 
                                    case "all":
                                        ctx.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                                        break;
                                    case "yes":
                                        if (_corsPrefixes == null) {
                                            break;
                                        }
                                        foreach (string corsPrefix in _corsPrefixes) {
                                            Uri pref = new Uri(corsPrefix);
                                            if (pref.Host == ctx.Request.Url.Host && pref.Port == ctx.Request.Url.Port && pref.Scheme == ctx.Request.Url.Scheme) {
                                                ctx.Response.Headers.Add("Access-Control-Allow-Origin", corsPrefix);
                                                break;
                                            }
                                        }
                                        break;
                                }

                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                            //catch { } // suppress any exceptions
                            finally
                            {
                                // always close the stream
                                ctx.Response.OutputStream.Close();
                            }
                        }, _listener.GetContext());
                    }
                }
                catch { } // suppress any exceptions
            });
        }

        public void Stop()
        {
            _listener.Stop();
            _listener.Close();
        }
    }

}
