﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelSwitchCS
{
    //"ckey","sid","tid","pmtpid","pcrpid","vidpid","audpid","ac3pid","ttxpid","ecmpid","channr","flags","servicetype","serviceprovidername","servicename","sat_id","dev_id","frequency","symbolrate","polarity","fec","modulation","nid","source_id","growth1","growth2","growth3","default_language","growth4","plsmode","plscode"

    public struct BriefChannelEntity
    {
        private string _name;

        public int ckey { get; set; }
        public int satid { get; set; }
        public int sid { get; set; }
        public int tid { get; set; }
        public int nid { get; set; }
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public byte[] bname
        {
            set
            {
                Encoding win1252 = Encoding.GetEncoding(1252);
                _name = win1252.GetString(value);
            }
        }
    }

    // catid, name, catorder
    public struct BriefCategoryEntity
    {
        private string _name;

        public int catid { get; set; }
        public int catorder { get; set; }
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public byte[] bname
        {
            set
            {
                Encoding win1251 = Encoding.GetEncoding(1251);
                _name = win1251.GetString(value);
            }
        }
    }

}
