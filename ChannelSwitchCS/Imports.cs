﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace ChannelSwitchCS
{
    public struct Externals
    {
        // system dlls
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern bool AppendMenu(IntPtr hMenu, uint uFlags, uint uIDNewItem, string lpNewItem);

        [DllImport("user32.dll")]
        public static extern IntPtr CreatePopupMenu();

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsWindow(IntPtr hWnd);

        // c++ helper dll
        [DllImport("cpphelper.dll")]
        public static extern long GetChannel(ref AddOnSettings settings, IntPtr pInt, uint uiAddOnId, out CHANNEL030 chn, IntPtr pDev);

        [DllImport("cpphelper.dll")]
        public static extern long ChangeChannel(ref AddOnSettings settings, IntPtr pInt, uint uiAddOnId, ref CHANNEL030 chn, IntPtr pDev);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate long t_OnMenuClick(uint uiId);

        [DllImport("cpphelper.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern long RegisterMenuEvents(ref AddOnSettings settings, IntPtr pInt, uint uiAddOnId, t_OnMenuClick fOnMenuClick);
    }

    class Imports
    {
    }
}
