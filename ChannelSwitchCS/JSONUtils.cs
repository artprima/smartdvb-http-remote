﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ChannelSwitchCS
{
    class JSONUtils
    {
        public struct JSEND
        {
            public string status;
            public object data;
            public string message;
            public int code;
        }

        public static string Error(Exception e, int Code = 0, object Data = null)
        {
            return JsonConvert.SerializeObject(new JSEND{
                status = "error",
                message = e.ToString(),
                code = Code,
                data = Data
            });
        }

        public static string Success(object Data, string FieldName = "list")
        {
            var dict = new Dictionary<string, object>();
            dict[FieldName] = Data;
            return JsonConvert.SerializeObject(new JSEND
            {
                status = "success",
                data = dict
            });
        }

        public static string Fail(object Data, string FieldName = "info")
        {
            var dict = new Dictionary<string, object>();
            dict[FieldName] = Data;
            return JsonConvert.SerializeObject(new JSEND
            {
                status = "fail",
                data = dict
            });
        }

        public static string FailNotFound()
        { 
            return Fail("Not Found");
        }
    }
}
