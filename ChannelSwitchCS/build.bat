@echo off
cd ..
"C:\Program Files (x86)\MSBuild\12.0\Bin\msbuild" ChannelSwitchCS.sln /p:configuration=release /t:Rebuild
if errorlevel 1 (
	echo Build failed. Exiting...
	exit
)

cd ChannelSwitchCS

copy /Y bin\Release\ChannelSwitchCS.dll setup\
xcopy /S /E /Y AddOnData\ChannelSwitchCS setup\ChannelSwitchCS\
cd setup
zip ChannelSwitchCS-demo.zip ChannelSwitchCS.dll ChannelSwitchCS\* ChannelSwitchCS\skins\default\*
rmdir /S /Q ChannelSwitchCS
del /Q ChannelSwitchCS.dll
cd ..

