﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SQLite;

namespace ChannelSwitchCS
{
    public class Repositories
    {
        private SQLiteConnection db;

        public Repositories(SQLiteConnection db)
        { 
            this.db = db;
        }

        public IEnumerable<BriefChannelEntity> GetChannels(int category = 0)
        {
            IEnumerable<BriefChannelEntity> res = null;
            
            try {
                if (category > 0) {
                    res = db.Query<BriefChannelEntity>("select ckey as ckey, sat_id as satid, sid as sid, tid as tid, nid as nid, ServiceName as bname from channels as t1 left join favorites as t2 on t1.ckey=t2.channelsid where categoryid=?", category);
                }
                else
                {
                    res = db.Query<BriefChannelEntity>("select ckey as ckey, sat_id as satid, sid as sid, tid as tid, nid as nid, ServiceName as bname from channels");
                }
            }
            catch (Exception e) { 
                MessageBox.Show(e.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return res;
        }

        public BriefChannelEntity? GetChannel(int id)
        {
            BriefChannelEntity? res = null;

            res = db.Query<BriefChannelEntity>("select ckey as ckey, sat_id as satid, sid as sid, tid as tid, nid as nid, ServiceName as bname from channels where ckey=?", id).First();

            return res;
        }

        public BriefChannelEntity? GetChannel(int satid, int sid, int tid, int nid)
        {
            BriefChannelEntity? res = null;

            //MessageBox.Show("select ckey as ckey, sat_id as satid, sid as sid, tid as tid, nid as nid, ServiceName as bname from channels where sat_id=? AND sid=? AND tid=? AND nid=? " + satid.ToString() + " " + sid.ToString() + " " + tid.ToString() + " " + nid.ToString() + " ");
            res = db.Query<BriefChannelEntity>("select ckey as ckey, sat_id as satid, sid as sid, tid as tid, nid as nid, ServiceName as bname from channels where sat_id=? AND sid=? AND tid=? AND nid=?", satid, sid, tid, nid).First();

            return res;
        }

        public IEnumerable<BriefCategoryEntity> GetCategories()
        {
            IEnumerable<BriefCategoryEntity> res = null;

            try
            {
                res = db.Query<BriefCategoryEntity>("select catid as catid, catorder as catorder, name as bname from categories");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return res;
        }

        public BriefCategoryEntity? GetCategory(int id)
        {
            BriefCategoryEntity? res = null;

            res = db.Query<BriefCategoryEntity>("select catid as catid, catorder as catorder, name as bname from categories where catid=?", id).First();

            return res;
        }

    }
}
