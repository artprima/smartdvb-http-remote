﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;
using SQLite;
using System.Windows.Forms;

namespace ChannelSwitchCS
{
    public class HTTPResponder
    {
        private SQLiteConnection db;
        private AddOnSettings settings;
        private IntPtr pInt;
        private uint uiAddOnId;

        // repos
        private Repositories repo;

        private string htmlData;


        public HTTPResponder(ref AddOnSettings settings, IntPtr pInt, uint uiAddOnId, SQLiteConnection db)
        {
            //string appdata = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string selfpath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string appdata = System.IO.Path.GetDirectoryName(selfpath);
            string skin = System.IO.Path.Combine(appdata, "AddOns\\ChannelSwitchCS\\skins\\default\\index.html");
            this.htmlData = System.IO.File.ReadAllText(skin);

            this.db = db;
            this.settings = settings;
            this.pInt = pInt;
            this.uiAddOnId = uiAddOnId;

            this.repo = new Repositories(db);
        }

        private delegate object DynamicFunc(params object[] parameters);
        private DynamicFunc MakeDynamicFunc(object target, MethodInfo method)
        {
            return par => method.Invoke(target, par);
        }

        private string HyphensToCamelCase(string data)
        {
            StringBuilder builder = new StringBuilder();
            string[] atoms = data.Split('-');
            // convert hyphens to CamelCase
            foreach (string atom in atoms)
            {
                if (atom.Length > 0)
                {
                    builder.Append(char.ToUpper(atom[0]) + atom.Substring(1));
                }
            }

            return builder.ToString();
        }

        public string Router(HttpListenerContext ctx)
        {
            Uri url = ctx.Request.Url;

            // we shouldn't have less than 2, but just to make sure
            if (url.Segments.Count() < 2)
            {
                ctx.Response.StatusCode = 404;
                return "<HTML><BODY>Not Found</BODY></HTML>";
            }
            // this is a "home page"
            if (url.Segments.Count() < 3)
            {
                return this.htmlData;
            }

            // remaining cases are actions (or not found)

            string action = url.Segments[2].Trim('/');
            //string methodName = char.ToUpper(action[0]) + action.Substring(1) + "Response";

            StringBuilder methodNameBuilder = new StringBuilder();

            // prefix as method name GET_, POST_, etc.
            methodNameBuilder.Append(ctx.Request.HttpMethod.ToString() + "_");

            // convert hyphens to CamelCase
            methodNameBuilder.Append(HyphensToCamelCase(action));

            // Method Suffix
            methodNameBuilder.Append("Response");

            // Obtain method info by name
            MethodInfo actionMethod = typeof(HTTPResponder).GetMethod(methodNameBuilder.ToString());

            // Check if the method exists
            if (actionMethod != null)
            {
                DynamicFunc fn;
                // make a function from the method
                try { 
                    fn = MakeDynamicFunc(this, actionMethod);
                }
                catch (Exception e) { 
                    MessageBox.Show(e.ToString());
                    ctx.Response.StatusCode = 500;
                    return "Error 500";
                }

                string res = "";
                // call a function an return its result
                try {
                    res = fn(ctx, url.Segments.Skip(3).ToArray()) as string;
                }
                catch (Exception e) {
                    MessageBox.Show(e.ToString());
                    ctx.Response.StatusCode = 500;
                    return "Error 500";
                }

                return res;
            }

            // finally, if no method found, return error 404
            ctx.Response.StatusCode = 404;
            return "<HTML><BODY>Not Found (at all)</BODY></HTML>";
        }

        private string ChannelsResponse(HttpListenerContext ctx, string[] segments, int category=0)
        {
            if (segments.Count() > 0)
            {
                int numVal;
                try
                {
                    numVal = Convert.ToInt32(segments[0].Trim(' ', '/'));
                }
                catch (FormatException e)
                {
                    ctx.Response.StatusCode = 400;
                    return JSONUtils.Fail("Channel id must be integer", "info");
                }

                BriefChannelEntity? channel = null;
                try
                {
                    channel = repo.GetChannel(numVal);
                }
                catch (System.InvalidOperationException e)
                {
                    ctx.Response.StatusCode = 404;
                    return JSONUtils.FailNotFound();
                }
                catch (Exception e)
                {
                    ctx.Response.StatusCode = 500;
                    return JSONUtils.Error(e);
                }

                return JSONUtils.Success(channel, "item");
            }

            IEnumerable<BriefChannelEntity> channels = repo.GetChannels(category);

            ctx.Response.ContentType = "application/json";

            return JSONUtils.Success(channels, "list");
        }

        public string GET_ChannelsResponse(HttpListenerContext ctx, string[] segments)
        { 
            return ChannelsResponse(ctx, segments);
        }

        public string GET_CategoriesResponse(HttpListenerContext ctx, string[] segments)
        {
            int categoryId = 0;
            if (segments.Count() > 0) {
                try
                {
                    categoryId = Convert.ToInt32(segments[0].Trim(' ', '/'));
                }
                catch (FormatException e)
                {
                    ctx.Response.StatusCode = 400;
                    return JSONUtils.Fail("Category id must be integer", "info");
                }
            }

            if (segments.Count() >= 2) // i.e. /1/channel[/[1]]
            {
                if (segments[1] != "channels") {
                    ctx.Response.StatusCode = 404;
                    return JSONUtils.FailNotFound();
                }
                return ChannelsResponse(ctx, segments.Skip(2).ToArray(), categoryId);
            } else if (segments.Count() == 1) {
                BriefCategoryEntity? category = null;
                try
                {
                    category = repo.GetCategory(categoryId);
                }
                catch (System.InvalidOperationException e)
                {
                    ctx.Response.StatusCode = 404;
                    return JSONUtils.FailNotFound();
                }
                catch (Exception e)
                {
                    ctx.Response.StatusCode = 500;
                    return JSONUtils.Error(e);
                }

                if (category == null || !category.HasValue)
                {
                    ctx.Response.StatusCode = 404;
                    return JSONUtils.FailNotFound();
                }

                return JSONUtils.Success(category, "item");
            }

            IEnumerable<BriefCategoryEntity> categories = repo.GetCategories();

            ctx.Response.ContentType = "application/json";

            return JSONUtils.Success(categories, "list");
        }

        public string GET_ActiveChannelResponse(HttpListenerContext ctx, string[] segments)
        {
            BriefChannelEntity? mycn;
            CHANNEL030 chn;
            Externals.GetChannel(ref settings, pInt, uiAddOnId, out chn, IntPtr.Zero);

            //File.WriteAllBytes(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "SmartDVB\\test.bin"), chn.Satellite);

            //MessageBox.Show(JsonConvert.SerializeObject(chn));

            try {
                mycn = repo.GetChannel(chn.iSatID, chn.SID, chn.TID, chn.NID);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString());
                ctx.Response.StatusCode = 404;
                return JSONUtils.FailNotFound();
            }

            return JSONUtils.Success(mycn.Value, "channel");
        }
        
        public string POST_ActiveChannelResponse(HttpListenerContext ctx, string[] segments)
        {
            string text;
            using (var reader = new StreamReader(ctx.Request.InputStream,
                                                 ctx.Request.ContentEncoding))
            {
                text = reader.ReadToEnd();
            }

            BriefChannelEntity mycn;
            try {
                mycn = JsonConvert.DeserializeObject<BriefChannelEntity>(text);
            }
            catch (Exception e) { 
                ctx.Response.StatusCode = 400;
                return JSONUtils.Fail(e);
            }

            try {
                mycn = repo.GetChannel(mycn.ckey).Value;
            } catch (Exception e) {
                ctx.Response.StatusCode = 404;
                return JSONUtils.FailNotFound();
            }

            CHANNEL030 chn = new CHANNEL030
            {
                iSatID = (ushort)mycn.satid,
                SID = (ushort)mycn.sid,
                TID = (ushort)mycn.tid,
                NID = mycn.nid
            };

            long result = Externals.ChangeChannel(ref settings, pInt, uiAddOnId, ref chn, IntPtr.Zero);

            return JSONUtils.Success(result, "result");
        }

        

    }

}
