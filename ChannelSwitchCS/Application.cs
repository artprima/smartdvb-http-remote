﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RGiesecke.DllExport;
using System.Runtime.InteropServices;
using System.Net;
using System.Threading;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json;
using SQLite;

using HMENU = System.IntPtr;

namespace ChannelSwitchCS
{
    public class Application
    {
        private static IntPtr pInt;
        private static AddOnSettings settings;
        private static uint uiAddOnId;
        private static WebServer ws;
        private static List<uint> menus = new List<uint>();
        private static HTTPResponder responder;
        private static Config config;

        //private static string dbfile;
        private static SQLiteConnection db;


        // Note: this method will be called from a different thread!
        private static long OnMenuClick(uint uiId)
        {
            //CHANNEL030 ch;

            //int i=0;

            //if (uiId == menus[i++])
            //{
            //    CHANNEL030 mycn = new CHANNEL030();
            //    Externals.GetChannel(ref settings, pInt, uiAddOnId, out ch, IntPtr.Zero);
            //    //string serialized = JsonConvert.SerializeObject(ch);

            //    mycn.SID = 30;
            //    mycn.TID = 1400;
            //    mycn.NID = 318;
            //    mycn.iSatID = 130;

            //    Externals.ChangeChannel(ref settings, pInt, uiAddOnId, ref mycn, IntPtr.Zero);

            //    MessageBox.Show("changed2");
            //}

            return 0;
        }

        private static void RunHTTPServer()
        {
            Application.responder = new HTTPResponder(ref settings, pInt, uiAddOnId, db);
            
            List<string> prefixes = new List<string>();

            StringBuilder locationBuilder = new StringBuilder();
            foreach (HostPort value in config.BindTo)
            {
                locationBuilder.Append("http://");
                locationBuilder.Append(value.host).Append(":").Append(value.port);
                locationBuilder.Append("/control/");
                prefixes.Add(locationBuilder.ToString());
                locationBuilder.Clear();
            }

            Application.ws = new WebServer(responder.Router, config.AllowCors, config.CorsPrefixes, prefixes.ToArray());
            Application.ws.Run();
        }

        private static void SetupMenu(ref AddOnSettings settings)
        {
            //HMENU menu = Externals.CreatePopupMenu();
            //menus.Add(settings.idmenustart++);
            //Externals.AppendMenu(menu, 0x00000000, menus.Last(), "Debug3");
            //settings.menu = menu;
            //Externals.RegisterMenuEvents(ref settings, pInt, uiAddOnId, Application.OnMenuClick);
        }


        [DllExport("RegisterAddOn", CallingConvention = CallingConvention.Cdecl)]
        public static bool RegisterAddOn(uint uiAddOnId, IntPtr pInt, ref AddOnSettings settings)
        {
            //MessageBox.Show("Plugin registered"); // debug only

            Application.pInt = pInt;
            Application.uiAddOnId = uiAddOnId;
            settings.name = "HTTP Remote";
            Application.SetupMenu(ref settings);

            //string appdata = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string selfpath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string appdata = System.IO.Path.GetDirectoryName(selfpath);
            string dbfile = System.IO.Path.Combine(appdata, "smartdvb.db");
            ConfigProcessor configProcessor = new ConfigProcessor(System.IO.Path.Combine(appdata, "AddOns\\ChannelSwitchCS\\config.yml"));
            config = configProcessor.GetConfig();

            try {
                Application.db = new SQLiteConnection(dbfile, SQLiteOpenFlags.ReadOnly);
            }
            catch (Exception e) {
                MessageBox.Show(e.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            Application.settings = settings;
            Application.RunHTTPServer();

            return true;
        }

        [DllExport("StopAddOn", CallingConvention = CallingConvention.Cdecl)]
        public static bool StopAddOn(int left, int right)
        {
            Application.ws.Stop();

            return true;
        }
    }
}
