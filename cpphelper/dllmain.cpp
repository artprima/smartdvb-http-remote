// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "Channel.h"
#include "AddOnInterfaces.h"
#include "resource.h"
#include <sstream>
#include <string>

#define ADDON_API __declspec(dllexport)

typedef HRESULT (*t_OnMenuClick)(UINT uiId);

extern "C" {
    HRESULT ADDON_API __cdecl GetChannel(const AddOnSettings &settings, IAddOnInterfaces *pInt, UINT uiAddOnId, CHANNEL030 *chn, DeviceSettings *pDev = NULL);
    HRESULT ADDON_API __cdecl ChangeChannel(const AddOnSettings &settings, IAddOnInterfaces *pInt, UINT uiAddOnId, CHANNEL030 &chn, DeviceSettings *pDev = NULL);
    HRESULT ADDON_API __cdecl RegisterMenuEvents(const AddOnSettings &settings, IAddOnInterfaces *pInt, UINT iAddOnId, t_OnMenuClick fOnMenuClick);
}

HMODULE g_hModule;

BOOL APIENTRY DllMain(HMODULE hModule,
                      DWORD  ul_reason_for_call,
                      LPVOID lpReserved
                      )
{
    switch (ul_reason_for_call)
    {
        case DLL_PROCESS_ATTACH:
        case DLL_THREAD_ATTACH: g_hModule = hModule;
            break;
        case DLL_THREAD_DETACH:
        case DLL_PROCESS_DETACH:
            break;
    }
    return TRUE;
}

class CMyEvents : public IAddOnChnEvents, public IAddOnMenuEvents
{
private:
    t_OnMenuClick pOnMenuClick;
public:
    CMyEvents() {
    }
    CMyEvents(t_OnMenuClick fOnMenuClick) {
        /*
        const int i = 3;
        std::ostringstream s;
        s << sizeof(ESTREAM[MAX_ES_COUNT]);
        const std::string i_as_string(s.str());
        MessageBox(0, i_as_string.c_str(), "", 0);
        */
        pOnMenuClick = fOnMenuClick;
    }

    HRESULT  OnMenuClick(UINT uiId) {
        if (pOnMenuClick) {
            return pOnMenuClick(uiId);
        }
        return 0;
    }

    HRESULT OnBeforeChangeChannel(CHANNEL030 &chn, DeviceSettings *pDev) {
        return 0;
    }
    HRESULT OnAfterChangeChannel(CHANNEL030 &chn, DeviceSettings *pDev) {
        return 0;
    }

    HRESULT OnFilter(UINT PID, BYTE *pPacket, WORD wSize, DeviceSettings *pDev) {
        return 0;
    }

};

//long GetChannel(IAddOnInterfaces *pInt, CHANNEL &chn)
HRESULT GetChannel(const AddOnSettings &settings, IAddOnInterfaces *pInt, UINT uiAddOnId, CHANNEL030 *chn, DeviceSettings *pDev)
{
    return pInt->GetChannel(uiAddOnId, chn, pDev);
}

//long GetChannel(IAddOnInterfaces *pInt, CHANNEL &chn)
HRESULT ChangeChannel(const AddOnSettings &settings, IAddOnInterfaces *pInt, UINT uiAddOnId, CHANNEL030 &chn, DeviceSettings *pDev)
{
    //return pInt->ChangeChannel(uiAddOnId, chn, pDev);
    SchedulerTask st;
    st.action = ACTIONVIEW;
    st.type = TYPEONCE;
    st.sid = chn.SID;
    st.tid = chn.TID;
    st.nid = chn.NID;
    st.satid = chn.iSatID;
    COPYDATASTRUCT cds;
    cds.dwData = 1;
    cds.cbData = sizeof(SchedulerTask);
    cds.lpData = &st;

    // get OWNER window due to bug in API
    HWND hwndMainWindow = GetWindow(settings.hwnd, GW_OWNER);

    return SendMessage(hwndMainWindow, WM_COPYDATA, (WPARAM)hwndMainWindow, (LPARAM)(LPVOID)&cds);
}

HRESULT RegisterMenuEvents(const AddOnSettings &settings, IAddOnInterfaces *pInt, UINT iAddOnId, t_OnMenuClick fOnMenuClick)
{
    CMyEvents *pEvents = new CMyEvents(fOnMenuClick);
    return pInt->RegisterMenuEvents(iAddOnId, pEvents);
}

