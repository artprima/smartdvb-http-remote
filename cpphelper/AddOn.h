typedef BOOL (*RegisterAddOnFunc)(UINT iAddOnId, IAddOnInterfaces *pInt, AddOnSettings &settings);
typedef BOOL (*StopAddOnFunc)(UINT iAddOnId);

// for dll RegisterAddOn(UINT id, IAddOnInterfaces *pInt)
// for module processing RegisterOSDEvents(UINT iAddOnId, IAddOnOSDEvents *pEvents);

typedef struct _AddOnData{
	RegisterAddOnFunc funcRegister;
	StopAddOnFunc funcStop;
	HINSTANCE hInst;
	UINT id;
	TCHAR name[256];
	TCHAR path[512];
	UINT pids[8192];
	IAddOnOSDEvents *pOSDEvents;
} AddOnData;

class CAddOnProcessor : public IAddOnInterfaces, public IAddOnOSDEvents
{
private:
public:
	CSimpleArray<AddOnData> m_AddOnData;
	BOOL StartAddOns();
	BOOL StopAddOns();

	// interface functions

	// 8192 for full TS stream
	HRESULT AddFilter(UINT uiAddOnId, UINT pid); 
	HRESULT RemoveFilter(UINT uiAddOnId, UINT pid);
	HRESULT CreateOSDElement(UINT uiAddOnId,  OSDWindowType type, ULONG &ulId, OSDWindowInfo &info);
	HRESULT ChangeChannel(UINT uiAddOnId,  CHANNEL &chn);
	HRESULT RegisterOSDEvents(UINT iAddOnId, IAddOnOSDEvents *pEvents);
	HRESULT OnClick(UINT uiAddOnId, OSDWindowInfo &info, int x, int y);

};

