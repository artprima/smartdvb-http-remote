#if !defined(AFX_STCH_H__FD48F021_DBCC_413E_AF3E_CAB1ABB1E88A__INCLUDED_)
#define AFX_STCH_H__FD48F021_DBCC_413E_AF3E_CAB1ABB1E88A__INCLUDED_

enum FECType {
	SDFEC_AUTO=0,
	SDFEC_1_2,
	SDFEC_2_3,
	SDFEC_3_4,
	SDFEC_4_5,
	SDFEC_5_6,
	SDFEC_6_7,
	SDFEC_7_8,
	SDFEC_8_9,
	SDFEC_9_10
};


enum MODULATIONType {
	MOD_QPSK=0,
	MOD_8PSK,
	MOD_NBCQPSK,
	MOD_AUTO,
	MOD_QAM16,
	MOD_QAM32,
	MOD_QAM64,
	MOD_QAM128,
	MOD_QAM256,
	MOD_VSB8,
	MOD_VSB16,
	MOD_TURBO_8PSK,
	MOD_TURBO_QPSK,
	MOD_TURBO_16QAM,
	MOD_16APSK,
	MOD_32APSK
};


enum LNBType {
	LNB_KULINEAR=0,
	LNB_KUCIRCULAR,
	LNB_CBAND,
	LNB_NA_BANDSTACKED_DP_DBS, // na bandstacked dishpro ku-hi DBS circular
	LNB_NA_BANDSTACKED_DP_FSS, // na bandstacked dishpro ku-lo FSS linear
	LNB_NA_BANDSTACKED_DBS, // na bandstacked ku-hi DBS circular
	LNB_NA_BANDSTACKED_FSS, // na bandstacked ku-lo FSS linear
	LNB_NA_BANDSTACKED_C, // bandstacked c
	LNB_NA_LEGACY,		 // na legacy circular (i think not sure it's always circular)
	LNB_UNICABLE		 // na legacy circular (i think not sure it's always circular)
};

typedef struct EStream{
	unsigned char type;
	unsigned short PID;
	char language[4];
	BOOL bIsAC3;
	BOOL bIsTTX;
	BOOL bIsAAC;
	BOOL bIsDVBSUB;
    BOOL bIsHEVC;
} ESTREAM;

typedef struct Transponder {
	unsigned int frequency;
	unsigned int symbolrate;
	unsigned char polarity;
	unsigned int fec;      // transponderfec values
	unsigned int id;
} TRANSPONDER;

struct SyncTransponder {
	unsigned int frequency;
	unsigned int symbolrate;
	unsigned char polarity;
	unsigned int fec;      // transponderfec values
	char strmodulation[256];
	BOOL bDifferent, bNew, bDeleted, bUpdated, bEmpty, bOld, bIdentical;
	BOOL bProcessed;
	
};

typedef struct Satellite {
	char Name[128];
	char Pos[10];
	unsigned short ID;
	BYTE DevNr;
	int lof1, lof2, sw;
	int diseqctype1, diseqclnb1;
	char rawcmd[256];
	int diseqctype2, diseqclnb2;
	char rawcmd2[256];
	unsigned short lnbtype;
	int positionertype;
	int positionerpos;
	int positionerangle;
	char rawcmdpositioner[256];
	BOOL b22khz;
	int diseqc1tx, diseqc2tx, positionertx,
		diseqc1gap, diseqc2gap, positionergap,
		lnbdelay;
} SATELLITE;

typedef struct CASystem {
	unsigned short CAType;
	unsigned short ECM;
	unsigned short EMM;
} CASYSTEM;

#define MAX_ES_COUNT 32
#define MAX_NAME_LEN 64
#define MAX_CATEGORY_LEN 32
#define MAX_CA_COUNT 16

typedef struct ExtraChanInfo {
	unsigned int iFilePos;
	unsigned short category;
} EXTRACHANINFO;

typedef struct Channel030 {
	unsigned short SID;
	unsigned short TID;
	unsigned short PMT_PID;
	unsigned short PCR_PID;
	unsigned short VID_PID;
	unsigned short AUD_PID;
	unsigned short AC3_PID;
	unsigned short TTX_PID;
	unsigned short ECM_PID;
	unsigned short DVBSUB_PID; // dvb subtitling pid, growth1 in db
	unsigned short CH;
	int NID;
	ESTREAM ES[MAX_ES_COUNT];
	unsigned short iESCount;	// # of elementary streams
	BOOL bHasService;			// we have an sdt entry for this
	BOOL bHasCA;				// true if we have at least 1 CA
	BOOL bHasAC3;				// true if we have at least 1 AC3
	BOOL bHasH264;				// true if we have h264 video channel
	BOOL bHasDVBSUB;				// true if we have h264 video channel
	BOOL bDeleted;
	BOOL bUseAC3;
	BOOL bUseAAC;
	BOOL bAutoFec;
	int iSourceId;		// atsc source id, dev_id in database
	unsigned char ServiceType;
	char ServiceProviderName[MAX_NAME_LEN+1];
	char ServiceName[MAX_NAME_LEN+1];
	unsigned char iCACount;	    // conditional access count
	CASYSTEM CASystem[MAX_CA_COUNT];
	unsigned short iSatID;
	TRANSPONDER Transponder;
	EXTRACHANINFO extra;
	SATELLITE Satellite;
	unsigned short Modulation;
	unsigned short ttxsubtitlepage;
	short DefaultCodepage;
	TCHAR DefaultEPGLang[4];
} CHANNEL030;


typedef struct FavChannel {
	CHANNEL030 chan;
	int favid;
	char favcategory[256];
	int catid;
} FAVCHANNEL;

typedef struct RemoteChannel {
	CHANNEL030 chan;
	int remoteid;
} REMOTECHANNEL;


#endif