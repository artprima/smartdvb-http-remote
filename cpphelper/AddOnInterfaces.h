#if !defined(AFX_ADDONINTERFACES_H__FD48F021_DBCC_413E_AF3E_CAB1ABB1E88A__INCLUDED_)
#define AFX_ADDONINTERFACES_H__FD48F021_DBCC_413E_AF3E_CAB1ABB1E88A__INCLUDED_

#include "StdAfx.h"

// popup mnu strt id 
#define ADDONS_START_MENU_ID 25000

// for dll RegisterAddOn(UINT id, IAddOnInterfaces *pInt)
// for module processing RegisterOSDEvents(UINT iAddOnId, IAddOnOSDEvents *pEvents);
// for dll StopAddOn(UINT id)

typedef struct _AddOnSettings{
	HINSTANCE hInst; // SmartDVB application instance
	HWND hwnd;
	TCHAR name[256]; // addon name
	HMENU menu;      // addon popup menu
	WORD idmenustart;
} AddOnSettings;

typedef struct _DeviceSettings {
	TCHAR tunername[256];
	TCHAR modulename[256];
	TCHAR sId[64];
} DeviceSettings;

typedef enum _OSDWindowType {
	RELATIVE_WINDOW = 0
} OSDWindowType;

typedef struct _OSDWindowInfo {
	RECT r;
	ULONG idWindow;
	BOOL bAlt;
	BOOL bCtrl;
	TCHAR sKey[1];
} OSDWindowInfo;

class IAddOnOSDEvents {
public:
	virtual  HRESULT OnClick(OSDWindowInfo &info, int x, int y) = 0;
	virtual  HRESULT OnMouseMove(OSDWindowInfo &info, int x, int y) = 0;
};

class IAddOnChnEvents {
public:
	virtual HRESULT OnBeforeChangeChannel(CHANNEL030 &chn, DeviceSettings *pDev=NULL) = 0;
	virtual HRESULT OnAfterChangeChannel(CHANNEL030 &chn, DeviceSettings *pDev=NULL) = 0;
	virtual  HRESULT OnFilter(UINT PID, BYTE *pPacket, WORD wSize, DeviceSettings *pDev=NULL) = 0;
};

class IAddOnMenuEvents {
public:
	virtual  HRESULT OnMenuClick(UINT uiId) = 0;
};

class IAddOnInterfaces {
public:
    virtual HRESULT AddSectionFilter(UINT uiAddOnId, UINT pid, BYTE *filter, BYTE *mask, BYTE length, DeviceSettings *pDev) = 0;
    //	virtual HRESULT AddSectionFilter(UINT uiAddOnId, UINT pid, BYTE filter, BYTE mask, DeviceSettings *pDev) = 0;
    virtual HRESULT RemoveSectionFilter(UINT uiAddOnId, UINT pid, DeviceSettings *pDev) = 0;
    virtual HRESULT AddFilter(UINT uiAddOnId, UINT pid, DeviceSettings *pDev = NULL) = 0;
    virtual HRESULT RemoveFilter(UINT uiAddOnId, UINT pid, DeviceSettings *pDev = NULL) = 0;
    virtual HRESULT GetChannel(UINT uiAddOnId, CHANNEL030 *chn, DeviceSettings *pDev = NULL) = 0;
    virtual HRESULT GetTransponder(UINT uiAddOnId, SATELLITE *sat, TRANSPONDER *t, DeviceSettings *pDev = NULL) = 0;
    virtual HRESULT SetTransponder(UINT uiAddOnId, SATELLITE sat, TRANSPONDER t, DeviceSettings *pDev = NULL) = 0;
    virtual HRESULT CreateOSDElement(UINT uiAddOnId, OSDWindowType type, ULONG &ulId, OSDWindowInfo &info) = 0;
    virtual HRESULT ChangeChannel(UINT uiAddOnId, CHANNEL030 &chn, DeviceSettings *pDev = NULL) = 0;
    virtual HRESULT RegisterOSDEvents(UINT iAddOnId, IAddOnOSDEvents *pEvents) = 0;
    virtual HRESULT RegisterMenuEvents(UINT iAddOnId, IAddOnMenuEvents *pEvents) = 0;
    virtual HRESULT RegisterChnEvents(UINT iAddOnId, IAddOnChnEvents *pEvents) = 0;
    virtual HRESULT DoDVBCmd(UINT iAddOnId, BYTE pCmd, UINT uiLen, DeviceSettings *pDev = NULL) = 0;
    //	virtual HRESULT DoDiseqc(UINT iAddOnId, CHANNEL &chn, DeviceSettings *pDev=NULL) = 0;
    virtual HRESULT DoDiseqc(UINT iAddOnId, TRANSPONDER &tr, SATELLITE &sat, DeviceSettings *pDev = NULL) = 0;
    virtual HRESULT RecordBusy(UINT iAddOnId, BOOL *bBusy, DeviceSettings *pDev = NULL) = 0;
    virtual HRESULT GetDeviceSettings(UINT iAddOnId, DeviceSettings &Dev) = 0;
    virtual HRESULT GetSignalStrength(UINT iAddOnId, LONG *lStrength, DeviceSettings *Dev) = 0;
    virtual HRESULT GetSignalQuality(UINT iAddOnId, LONG *lQuality, DeviceSettings *Dev) = 0;
    virtual HRESULT SetModulation(UINT iAddOnId, enum MODULATIONType, DeviceSettings *Dev) = 0;
    // this is sometimes needed before DoDiseqc calls for correct disecq information
    virtual HRESULT SetTransponderInfo(UINT iAddOnId, TRANSPONDER trans, int iLnbType, int iLOF1, int iLOF2, int iSW, BOOL b22khz, DeviceSettings *Dev) = 0;
};


#define ACTIONRECORD 1
#define ACTIONVIEW 2
#define ACTIONSTOP 3
#define ACTIONNONE 4
#define ACTIONGETCHANNEL 5

#define TYPEONCE 1
#define TYPEDAILY 2
#define TYPEWEEKLY 3

struct SchedulerTask {
    char taskname[256];
    char channel[256];
    SYSTEMTIME starttime;
    SYSTEMTIME nextrun;
    int duration;
    int atstart;
    int atend;
    int action;
    int type;
    BYTE days; // bit 1 set is monday, 2 is tuesday and so on
    BOOL wakeupsystem;
    BOOL renderless;
    SHORT timeout;
    int satid;
    int sid;
    int tid;
    int nid;
    UINT transcodebitrate;
    char resolution[64];
};

#endif